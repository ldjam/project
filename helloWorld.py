import pygame
from pygame import sprite
from collections import deque

pygame.init()

black = (0, 0, 0)
white = (255, 255, 255)
green = (0, 255, 0)
light_blue = (0, 128, 255)
red = (255, 0, 0)
purple = (255, 0, 255)
light_gray = (160, 160, 160)

screen_width = 1366
screen_height = 768
window = pygame.display.set_mode([screen_width, screen_height])
pygame.display.set_caption("Quantum Cowboy")

square = pygame.Surface((1,1))
clock = pygame.time.Clock()

backgroundImg = pygame.image.load("Assets\\floor.png")
background_width = 1417
background_height = 466
upper_boundary = (screen_height*0.5) #524
top_floor = 524

tile_pos = [
    [86, 214, 342, 470, 598, 726, 854],
    [80, 208, 336, 464, 592, 720, 848],
    [74, 202, 330, 458, 586, 714, 842],
    ]
            #332, 460, 588
lane_width = 12


cityImg = pygame.image.load("Assets\Setting.png")
def background(back_x_change):
    window.blit(backgroundImg, (back_x_change, upper_boundary))

def quitgame():
    pygame.quit()
    quit()

def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()


def controls():
    escape = True
    while escape:
        for event in pygame.event.get():
            print(event)
            if event.type == pygame.QUIT:
                quitgame()

        #window.fill(light_gray)
        button("LEFT", 80, 100, 200, 50, green, light_blue, gameintro)
        button("LEFT", 80, 100, 200, 50, green, light_blue, gameintro)

def settingsbar():
    escape = True
    while escape:
        for event in pygame.event.get():
            print(event)
            if event.type == pygame.QUIT:
                quitgame()

        window.fill(light_gray)
        button("Return to Menu", 80, 100, 200, 50, green, light_blue, gameintro)
        button("Controls", 80, 200, 200, 50, purple, light_blue, gameintro)

        pygame.display.update()
        clock.tick(15)


def button(msg, x, y, width, height, inactive_color, active_color, action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    print(click)
    if x + width > mouse[0] > x and y + height > mouse[1] > y:
        pygame.draw.rect(window, active_color, (x, y, width, height))
        if click[0] == 1 and action != None:
            action()
    else:
        pygame.draw.rect(window, inactive_color, (x, y, width, height))

    smallText = pygame.font.Font("freesansbold.ttf", 20)
    textSurf, textRect = text_objects(msg, smallText)
    textRect.center = ((x + (width/2)), (y + (height/2)))
    window.blit(textSurf, textRect)


class Character:

    def __init__(self, img, width, height, x, y, lane, column, x_change, y_change, maxHP, defense, strength):
        self.img = img
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        #        self.z = z
        self.lane = lane
        self.column = column
        self.x_change = x_change
        self.y_change = y_change
        #        self.z_change = z_change
        self.maxHP = maxHP
        self.hp = self.maxHP
        self.defense = defense
        self.strength = strength

    def update(self, x_change, y_change):
        #if 0 < (self.x + x_change) < (screen_width - self.width):
        self.x += x_change
#            self.x += x_change
        #if (top_floor - self.height*0.5) < (self.y + y_change) < (screen_height + 3):# - self.height):
        self.y += y_change #+ z_change*0.5
#            self.y += y_change
#            player.z += z_change
#        elif (screen_height * 0.5) < (y + z_change*0.5) < (screen_height - pc_height):
#            y += y_change + z_change*0.5
#            player.y += y_change
#            player.z += z_change
#        else:
#            y += y_change
#            player.y += y_change
        window.blit(self.img, (self.x, self.y))



class Enemy(Character):
    def __init__(self, img, width, height, x, y, lane, column, x_change, y_change, maxHP, defense, strength):
        super().__init__(img, width, height, x, y, lane, column, x_change, y_change, maxHP, defense, strength)
        self.img = pygame.image.load('Assets\placeholderCowboy.png')
        self.width = 138
        self.height = 138
        self.img = pygame.transform.scale(self.img, (self.width, self.height))
        self.img = pygame.transform.flip(self.img, True, False)
        self.pos = self.img.get_rect()

    def damage(self, target):
        dmg = self.strength - target.defense
        target.hp -= dmg


class Player(Character):

    def __init__(self, img, width, height, x, y, lane, column, x_change, y_change, maxHP, defense, strength, weapon):
        super().__init__(img, width, height, x, y, lane, column, x_change, y_change, maxHP, defense, strength)

        self.x = tile_pos[1][0]
        self.y = 460
        self.lane = 2
        self.column = 1
        self.index = 0
        self.walk = []
        self.walk.append(pygame.image.load('Assets\Cowboy1.png'))
        self.walk.append(pygame.image.load('Assets\Cowboy2.png'))
        self.walk.append(pygame.image.load('Assets\Cowboy3.png'))
        self.walk.append(pygame.image.load('Assets\Cowboy4.png'))
        self.walk.append(pygame.image.load('Assets\Cowboy5.png'))
        self.walk.append(pygame.image.load('Assets\Cowboy6.png'))

        self.width = 138
        self.height = 138
        self.weapon = weapon
        self.img = pygame.transform.scale(self.walk[self.index], (138, 138))
        self.pos = self.img.get_rect()

    def set_pos(self, event_key, lane):
        if event_key == pygame.K_RIGHT:
            self.x += 128
        elif event_key == pygame.K_LEFT:
            self.x -= 128
        elif event_key == pygame.K_UP:
            if lane == 2:
                self.y = 332
                self.x += 6
            elif lane == 3:
                self.y = 460
                self.x += 6
        elif event_key == pygame.K_DOWN:
            if lane == 2:
                self.y = 588
                self.x -= 6
            elif lane == 1:
                self.y = 460
                self.x -= 6
        #window.blit(self.img, (self.x, self.y))

    def testwalk(self, event_key):
        if event_key == pygame.KEYDOWN:
            for x in range(0, len(self.walk)):
                self.img = self.walk[self.index]
                self.img = pygame.transform.scale(self.img, (138, 138))
                self.index = (self.index + 1) % len(self.walk)
                window.blit(self.img, (self.x, self.y))
                pygame.display.update()
        self.index = 0
        self.img = pygame.transform.scale(self.walk[self.index], (138, 138))
        window.blit(self.img, (self.x, self.y))
        pygame.display.update()

    def move_left(self):
        if 0 < self.column - 1 < 7:
            self.x_change = (tile_pos[self.lane - 1][self.column - 2] - self.x) / 10
            self.column -= 1

    def move_right(self):
        if 0 < self.column + 1 < 8:
            self.x_change = (tile_pos[self.lane - 1][self.column] - self.x) / 10
            self.column += 1

    def move_down(self):
        if 1 < self.lane <= 3:
            self.y_change = lane_width  # space_width / 10
            self.x_change = (tile_pos[self.lane - 2][self.column - 1] - self.x) / 10

    def move_up(self):
        if self.lane >= 1:
            self.y_change = -lane_width  # -space_width / 10
            self.x_change = (tile_pos[self.lane][self.column - 1] - self.x) / 10


    def attack(self, target):
        dmg = self.strength - target.defense
        target.hp -= dmg

class Weapon:

    def __init__(self, maxAmmo, firingSpeed):
        #self.player = player
        #self.img = img
        self.maxAmmo = maxAmmo
        self.currentAmmo = self.maxAmmo
        self.speed = firingSpeed


def gameintro():
    intro = True
    while intro:
        for event in pygame.event.get():
            print(event)
            if event.type == pygame.QUIT:
                quitgame()

        window.fill(white)
        largeText = pygame.font.Font('freesansbold.ttf', 115)
        TextSurf, TextRect = text_objects("Quantum Cowboy", largeText)
        TextRect.center = ((screen_width/2), (screen_height/2))
        window.blit(TextSurf, TextRect)

        button("GO!", 150, 450, 100, 50, green, light_blue, gameloop)
        button("QUIT!", 550, 450, 100, 50, red, light_blue, quitgame)
        button("OPTIONS", 350, 550, 100, 50, purple, light_blue, settingsbar)

        pygame.display.update()
        clock.tick(15)


def gameloop():
    xinit = 80      #(screen_width * 0.5)
    yinit = 460     #(screen_height * (2/3))
    z = (screen_height * 0.75)
    x_change = 0
    y_change = 0
    #z_change = 0

    placeholder = None
    pistol = Weapon(10, 10)
    p1 = Player(placeholder, placeholder, placeholder, xinit, yinit, 2, 1, x_change, y_change, 10, 10, 10, pistol)
    flying_spaghetti_monster = Enemy(pygame.image.load('Assets\placeholderCowboy.png'), 380, 380, tile_pos[1][5], yinit, 2, 6, 0, 0, 10, 5, 10)
    enemies = [flying_spaghetti_monster]
    defeated = []

    gameExit = False
    space_width = screen_height//6
    timer = 0
    last_action = 0
    moving = False
    action_queue = deque([])
    queue_limit = 7
    back_x_change = 0
    space_free = True

    window.blit(cityImg, (-200, -200))
    background(0)

    while not gameExit:
        #get all the user events
        for event in pygame.event.get():

            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                #print(pos)

            if event.type == pygame.QUIT:
                gameExit = True

            if timer - last_action < 10:
                break

            elif event.type == pygame.KEYDOWN:
                last_action = timer
                moving = True
                if event.key == pygame.K_LEFT:
                    try:
                        if action_queue[len(action_queue)-1] == 'R':
                            p1.move_left()
                            action_queue.pop()
                        elif enemies:
                            for fsm in enemies:
                                if ((fsm.column == p1.column - 1) and (fsm.lane == p1.lane)):
                                   space_free = False
                        elif len(action_queue) < queue_limit and p1.column > 1 and space_free:
                            p1.move_left()
                            action_queue.append('L')
                    except IndexError:
                        if p1.column > 1:
                            p1.move_left()
                            action_queue.append('L')

                elif event.key == pygame.K_RIGHT:
                    try:
                        if enemies:
                            for fsm in enemies:
                                if ((fsm.column == p1.column + 1) and (fsm.lane == p1.lane)):
                                    p1.attack(fsm)
                                    if fsm.hp <= 0:
                                        enemies.remove(fsm)
                                        defeated.append(fsm)
                        if action_queue[len(action_queue)-1] == 'L':
                            p1.move_right()
                            action_queue.pop()
                        elif len(action_queue) < queue_limit:
                            p1.move_right()
                            action_queue.append('R')
                            p1.testwalk(event.type)
                    except IndexError:
                        p1.move_right()
                        action_queue.append('R')
                        p1.testwalk(event.type)

                elif event.key == pygame.K_DOWN:
                    try:
                        if enemies:
                            for fsm in enemies:
                                if ((fsm.column == p1.column) and (fsm.lane == p1.lane - 1)):
                                   space_free = False
                        if (action_queue[len(action_queue)-1] == 'U') and ((p1.lane - 1) >= 1):
                            p1.move_down()
                            p1.lane -= 1
                            action_queue.pop()
                        elif (len(action_queue) < queue_limit) and ((p1.lane - 1) >= 1) and space_free:
                            p1.move_down()
                            p1.lane -= 1
                            action_queue.append('D')
                    except IndexError:
                        p1.move_down()
                        p1.lane -= 1
                        action_queue.append('D')

                elif event.key == pygame.K_UP:
                    try:
                        if enemies:
                            for fsm in enemies:
                                if ((fsm.column == p1.column) and (fsm.lane == p1.lane + 1)):
                                   space_free = False
                        if (action_queue[len(action_queue)-1] == 'D') and ((p1.lane + 1) <= 3):
                            p1.move_up()
                            p1.lane += 1
                            action_queue.pop()
                        elif (len(action_queue) < queue_limit) and ((p1.lane +1) <= 3) and space_free:
                            p1.move_up()
                            p1.lane += 1
                            action_queue.append('U')
                    except IndexError:
                        p1.move_up()
                        p1.lane += 1
                        action_queue.append('U')

                elif event.key == pygame.K_SPACE and len(action_queue) == queue_limit:
                    if p1.lane == 3:
                        p1.update(-p1.x + 74, 0)
                    elif p1.lane == 2:
                        p1.update(-p1.x + 80, 0)
                    elif p1.lane == 1:
                        p1.update(-p1.x + 86, 0)
                    p1.column = 1
                    back_x_change -= 768
                    background(back_x_change)
                    action_queue = ([])
#            if event.type == pygame.KEYUP:
#                if event.key == pygame.K_LEFT:
#                    x_change += 5
#                elif event.key == pygame.K_RIGHT:
#                    x_change += -5
#                elif event.key == pygame.K_DOWN:
#                    y_change += -5
#                elif event.key == pygame.K_UP:
#                    y_change += 5
#                elif event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT or event.key == pygame.K_UP or event.key == pygame.K_DOWN:
        if timer - last_action == 10:
            p1.x_change = 0
            p1.y_change = 0
            last_action = 0
            moving = False
            #space_free = True


        #window.fill(white)
        #background()
        #if ((xinit - space_width*3) < (p1.x + x_change*10) < (xinit + space_width*5)) and ((yinit - space_width*3) < (p1.y + y_change*10) < (yinit + space_width*3)):
        window.blit(cityImg, (-200, -200))
        background(back_x_change)
        p1.update(p1.x_change, p1.y_change)
        flying_spaghetti_monster.update(0, 0)
        #else:
        #    p1.update(0, 0)
        '''lane boundaries:
        1 : x has to be greater than 42, y should always be 588
        2 : x has to be greater than 80, y should always be 460
        3 : x has to be greater than 118, y should always be 332'''
        '''if p1.lane == 1:
            if 42 <= (p1.x + x_change) < screen_width and 588 <= (p1.y + y_change) <= 768:
                p1.update(x_change, y_change)
        elif p1.lane == 2:
            if 80 <= (p1.x + x_change) < screen_width and 460 <= (p1.y + y_change) <= 768:
                p1.update(x_change, y_change)
        if p1.lane == 3:
            if 118 <= (p1.x + x_change) < screen_width and 332 <= (p1.y + y_change) <= 768:
        '''

        #moving_msg = "moving: " + str(moving)
        #boundary = "bound: " + str(upper_boundary)
        p1Y = "Y Val: " + str(p1.y)
        p1Y_change = "Y Change: " + str(p1.y_change)
        p1X = "X Val: " + str(p1.x)
        p1X_change = "X Change: " + str(p1.x_change)
        player_lane = "Lane: " + str(p1.lane)
        floor_pos = "Column: " + str(p1.column)# + " " + str(tile_pos[0][1])
        p1hp = "P1 HP: " + str(p1.hp)
        p1def = "P1 DEF: " + str(p1.defense)
        p1str = "P1 STR: " + str(p1.strength)
        fsmhp = "FSM HP: " + str(flying_spaghetti_monster.hp)
        fsmdef = "FSM DEF: " + str(flying_spaghetti_monster.defense)
        fsmstr = "FSM STR: " + str(flying_spaghetti_monster.strength)
        space_free_indicator = "space free: " + str(space_free)
        #in_bounds = "in bounds: " + str((upper_boundary - p1.height*0.5) < (p1.y + y_change) < (screen_height - p1.height))
        #scrheight = "scrheight: " + str(screen_height)
        button(str(action_queue), 20, 70, 100, 50, green, red)
        #button(boundary, 20, 20, 100, 50, light_blue, red)
        button(p1Y, 20, 120, 100, 50, purple, red)
        button(p1Y_change, 300, 120, 100, 50, purple, red)
        button(p1X, 20, 170, 100, 50, light_blue, red)
        button(p1X_change, 300, 170, 100, 50, light_blue, red)
        button(player_lane, 500, 200, 100, 50, green, red)
        button(floor_pos, 500, 100, 100, 50, green, red)

        button(p1hp, 700, 120, 100, 50, green, red)
        button(p1def, 700, 170, 100, 50, light_blue, red)
        button(p1str, 700, 220, 100, 50, purple, red)

        button(fsmhp, 900, 120, 100, 50, green, red)
        button(fsmdef, 900, 170, 100, 50, light_blue, red)
        button(fsmstr, 900, 220, 100, 50, purple, red)

        button(space_free_indicator, 500, 300, 100, 50, green, red)

        #button(in_bounds, 20, 170, 100, 50, green, red)
        #button(scrheight, 20, 220, 100, 50, light_blue, red)

        space_free = True
        pygame.display.update()
        timer += 1
        clock.tick(60)


#gameintro()
gameloop()
pygame.quit()

for x in range(0,3):
    for y in range(0, 2):
        print( str(tile_pos[x][0]))# + " " + str(tile_pos[0][y]))
quit()
